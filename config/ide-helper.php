<?php
return [
    'custom_db_types' => array(
        'postgresql' => array(
            "jsonb" => "json_array",
            "json" => "json_array"
        )
    )
];