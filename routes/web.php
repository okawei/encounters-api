<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app = $router;
$rest = function($path, $controller) use ($app)
{
    $app->get($path, $controller.'@index');
    $app->get($path.'/{id}', $controller.'@show');
    $app->post($path, $controller.'@store');
    $app->put($path.'/{id}', $controller.'@update');
    $app->delete($path.'/{id}', $controller.'@destroy');

    // This is to make blueagent happy
    $app->options($path, function(){
        return ['success'=>true];
    });
    $app->options($path.'/{id}', function(){
        return ['success'=>true];
    });
};
$router->get('/', function(){
    return ['success'=>true];
});
$router->group(['prefix'=>'api', 'middleware'=>['convertPartyCode']], function() use ($rest, $router){
    $rest('/monsters', 'MonstersController');
    $rest('/spells', 'SpellsController');
    $rest('/parties', 'PartiesController');
    $rest('/player_characters', 'PlayerCharactersController');
    $rest('/races', 'RacesController');
    $rest('/alignments', 'AlignmentsController');
    $rest('/encounters', 'EncountersController');
    $rest('/encounter/player_characters', 'EncounterPlayerCharactersController');
    $rest('/encounter/monsters', 'EncounterMonstersController');
    $rest('/events', 'EventsController');
    $rest('/classes', 'PlayerClassesController');
    $router->options('/encounters/{id}/roll_initiative', function(){
        return [
            'success'=>true
        ];
    });
    $router->post('/encounters/{id}/roll_initiative', 'EncountersController@rollInitiative');

    $router->get('/enumerators/all', 'EnumeratedController@getEnumerators');
    $router->get('/enumerators/{method}', 'EnumeratedController@getMapToMethod');
});


