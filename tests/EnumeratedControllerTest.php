<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EnumeratedControllerTest extends TestCase
{

    public function testItGetsAllMappings(){
        $response = $this->json('GET', '/api/enumerators/all');
        $this->assertResponseOk();
    }

    public function testItGetsASpecificMethod(){
        $response = $this->json('GET', '/api/enumerators/dice');
        $this->assertResponseOk();
    }
}
