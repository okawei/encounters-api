<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RacesControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListRaces()
    {
        $response = $this->json('GET', '/api/races');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'race'
            ]
        ]);
    }
}
