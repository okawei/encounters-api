<?php

use EncountersApi\Party;
use EncountersApi\PlayerCharacter;

class PlayerCharactersControllerTest extends TestCase
{
    const BASE_PATH = '/api/player_characters/';

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetPlayerCharacters()
    {
        $party = factory(Party::class)->create();
        $charactersToCreate = $this->faker->numberBetween(1, 5);
        $players = factory(PlayerCharacter::class, $charactersToCreate)
            ->create()
            ->each(function ($player) use ($party) {
                $player->party_id = $party->id;
                $player->save();
            });
        $response = $this->json('GET', self::BASE_PATH, [
            'party_code' => $party->hash
        ]);
        $response->assertResponseOk();
        $array_keys = array_keys($players->first()->getAttributes());
        if (($key = array_search('party_id', $array_keys)) !== false) {
            unset($array_keys[$key]);
        }
        $response->seeJsonStructure([
            '*'=> $array_keys
        ]);
    }

    public function testGetPlayerCharacter()
    {
        $player = factory(PlayerCharacter::class)->create();
        $response = $this->json('GET', self::BASE_PATH .$player->id, [
            'party_code' => $player->party->hash
        ]);
        $response->assertResponseOk();
        $playerKeys = array_keys(json_decode($player->first()->toJson(), true));
        $response->seeJsonStructure(
            $playerKeys
        );
    }

    public function testNotReturningPartyId()
    {
        $player = factory(PlayerCharacter::class)->create();
        $response = $this->json('GET', self::BASE_PATH .$player->id, [
            'party_code' => $player->party->hash
        ]);
        $response->assertResponseOk();
        $responseContent = json_decode($response->response->content());
        $this->assertTrue(!isset($responseContent->party_id));
    }

    public function testUpdatePlayerCharacter()
    {
        $player = factory(PlayerCharacter::class)->create();
        $updates = factory(PlayerCharacter::class)->make()->toArray();
        foreach ($updates as $key => $value) {
            if (rand(0, 10) < 7) {
                unset($updates[$key]);
            }
        }
        $response = $this->json('PUT', self::BASE_PATH .$player->id, $updates);
        $response->assertResponseOk();
    }

    public function testDeletePlayerCharacter()
    {
        $player = factory(PlayerCharacter::class)->create();
        $response = $this->json('DELETE', self::BASE_PATH .$player->id);
        $response->assertResponseOk();
    }

    public function testCreatePlayerCharacter()
    {
        $player = factory(PlayerCharacter::class)->make()->toArray();
        $player['party_code'] = factory(Party::class)->create()->hash;
        $response = $this->json('POST', self::BASE_PATH, $player);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->getContent());
        $this->assertNotNull(PlayerCharacter::find($responseData->id));
    }

    public function testNoPartyCodeResultsInError()
    {
        $this->json('GET', self::BASE_PATH)->assertResponseStatus(422);
    }
}
