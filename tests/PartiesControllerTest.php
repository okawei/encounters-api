<?php

class PartiesControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateParty()
    {
        $response = $this->json('POST', '/api/parties');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'id',
            'hash'
        ]);
    }

    public function testGetParty()
    {
        $party = factory(\EncountersApi\Party::class)->create();
        $response = $this->json('GET', '/api/parties/'.$party->hash);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'id',
            'hash'
        ]);
    }
}
