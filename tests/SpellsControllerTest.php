<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SpellsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetMonsters()
    {
        $response = $this->json('GET', '/api/spells');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'spell'
            ]
        ]);
    }

    public function testGetMonstersList()
    {
        $response = $this->json('GET', '/api/spells?list=true');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'name'
            ]
        ]);
    }
}
