<?php

use EncountersApi\Encounter;
use EncountersApi\EncounterPlayerCharacter;
use EncountersApi\PlayerCharacter;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EncounterPlayerCharactersControllerTest extends TestCase
{

    const BASE_PATH = '/api/encounter/player_characters/';

    public function testIndex()
    {
        $encounter = factory(Encounter::class)->create();
        $playerCharacter = factory(PlayerCharacter::class)->create([
            'party_id'=>$encounter->party_id
        ]);
        $encounterPlayerCharacter = factory(EncounterPlayerCharacter::class)->create([
            'encounter_id'=>$encounter->id,
            'player_character_id'=>$playerCharacter->id
        ]);
        $response = $this->json('GET', self::BASE_PATH, [
            'party_code'=>$encounter->party->hash,
            'encounter_id' => $encounter->id
        ]);
        $response->assertResponseOk();
        collect(json_decode($response->response->content()))->each(function ($playerCharacterCheck) use ($encounterPlayerCharacter) {
            $this->assertEquals($playerCharacterCheck->id, $encounterPlayerCharacter->id);
        });
    }

    public function testStore()
    {
        $encounter = factory(Encounter::class)->create();
        $playerCharacter = factory(PlayerCharacter::class)->create([
            'party_id'=>$encounter->party_id
        ]);
        $response = $this->json('POST', self::BASE_PATH, [
            'encounter_id' => $encounter->id,
            'party_code' => $encounter->party->hash,
            'player_character_id' => $playerCharacter->id
        ]);
        $response->assertResponseOk();
        $check = $encounter->playerCharacters()->get()->first();
        $this->assertEquals($check->player_character_id, $playerCharacter->id);
    }

    public function testShow()
    {
        $encounter = factory(Encounter::class)->create();
        $encounterPlayerCharacter = factory(EncounterPlayerCharacter::class)->create([
            'encounter_id'=>$encounter->id
        ]);

        $response = $this->json('GET', self::BASE_PATH.$encounterPlayerCharacter->id, [
            'encounter_id'=>$encounterPlayerCharacter->encounter_id,
            'party_code'=>$encounterPlayerCharacter->encounter->party->hash
        ]);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());

        $this->assertEquals($responseData->id, $encounterPlayerCharacter->id);
    }

    public function testUpdate()
    {
        $encounterPlayerCharacter = factory(EncounterPlayerCharacter::class)->create();
        $newFields = factory(EncounterPlayerCharacter::class)->make([
            'encounter_id'=>$encounterPlayerCharacter->encounter_id,
            'player_character_id'=>$encounterPlayerCharacter->player_character_id
        ])->toArray();
        $response = $this->json('PUT', self::BASE_PATH.$encounterPlayerCharacter->id, [
                'encounter_id'=>$encounterPlayerCharacter->encounter_id,
                'party_code' => $encounterPlayerCharacter->encounter->party->hash
            ] + $newFields);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());
        foreach ($newFields as $key => $field) {
            $this->assertEquals($responseData->$key, $field);
        }
    }

    public function testDelete()
    {
        $encounterPlayerCharacter = factory(EncounterPlayerCharacter::class)->create();
        $response = $this->json('DELETE', self::BASE_PATH.$encounterPlayerCharacter->id, [
            'encounter_id'=>$encounterPlayerCharacter->encounter_id,
            'party_code'=>$encounterPlayerCharacter->encounter->party->hash
        ]);
        $response->assertResponseOk();
        $this->assertTrue(EncounterPlayerCharacter::find($encounterPlayerCharacter->id) == null);
    }
}
