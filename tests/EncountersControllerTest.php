<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EncountersControllerTest extends TestCase
{
    public function testRollInitiative(){
        $party = factory(\EncountersApi\Party::class)->create();
        $encounter = factory(\EncountersApi\Encounter::class)->create([
            'party_id' => $party->id
            ]
        );
        $playerCharacter = factory(\EncountersApi\PlayerCharacter::class)->create([
            'party_id'=>$party->id
        ]);
        $encounterPlayerCharacter = factory(\EncountersApi\EncounterPlayerCharacter::class)->create([
            'player_character_id' => $playerCharacter->id,
            'encounter_id' => $encounter->id,
            'initiative' => 1
        ]);
        $encounterMonster = factory(\EncountersApi\EncounterMonster::class)->create([
            'monster_id' => \EncountersApi\Monster::inRandomOrder()->first()->id,
            'encounter_id' => $encounter->id,
            'initiative' => 1
        ]);
        $response = $this->json('POST', '/api/encounters/'.$encounter->id.'/roll_initiative', [
            'party_code' => $party->hash,
            'entities' => [
                [
                    'id' => $encounterMonster->id,
                    'type' => 'monster',
                    'initiative' => 10
                ],
                [
                    'id' => $encounterPlayerCharacter->id,
                    'type' => 'player_character',
                    'initiative' => 10
                ],
            ]
        ]);
        $response->assertResponseOk();
        $encounterPlayerCharacter = \EncountersApi\EncounterPlayerCharacter::find($encounterPlayerCharacter->id);
        $encounterMonster = \EncountersApi\EncounterMonster::find($encounterMonster->id);
        $this->assertEquals(10, $encounterPlayerCharacter->initiative);
        $this->assertEquals(10, $encounterMonster->initiative);
    }
}
