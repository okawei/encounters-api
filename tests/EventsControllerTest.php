<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventsControllerTest extends TestCase
{
    const BASE_PATH = '/api/events/';

    public function testIndex()
    {
        $encounter = factory(\EncountersApi\Encounter::class)->create();
        $event = factory(\EncountersApi\Event::class)->create([
            'encounter_id'=>$encounter->id
        ]);
        $response = $this->json('GET', self::BASE_PATH, [
            'party_code'=>$encounter->party->hash,
            'encounter_id' => $encounter->id
        ]);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());
        $this->assertEquals($event->id, $responseData[0]->id);
    }

    public function testStore()
    {
        $encounter = factory(\EncountersApi\Encounter::class)->create();
        $eventData = factory(\EncountersApi\Event::class)->make([
            'encounter_id'=>$encounter->id,
        ])->toArray();
        $response = $this->json('POST', self::BASE_PATH, [
            'party_code'=>$encounter->party->hash
            ] + $eventData);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());
        foreach ($eventData as $key => $datum) {
            $this->assertEquals($responseData->$key, $datum);
        }
    }

    public function testShow()
    {
        $encounter = factory(\EncountersApi\Encounter::class)->create();
        $event = factory(\EncountersApi\Event::class)->create([
            'encounter_id'=>$encounter->id
        ]);
        $response = $this->json('GET', self::BASE_PATH.$event->id, [
            'party_code'=>$encounter->party->hash,
            'encounter_id' => $encounter->id
        ]);
        $responseData = json_decode($response->response->content());
        $this->assertEquals($event->id, $responseData->id);
    }

    public function testDelete()
    {
        $encounter = factory(\EncountersApi\Encounter::class)->create();
        $event = factory(\EncountersApi\Event::class)->create([
            'encounter_id'=>$encounter->id
        ]);
        $response = $this->json('DELETE', self::BASE_PATH.$event->id, [
            'party_code'=>$encounter->party->hash,
            'encounter_id' => $encounter->id
        ]);
        $response->assertResponseOk();
        $this->assertNull(\EncountersApi\Event::find($event->id));
    }
}
