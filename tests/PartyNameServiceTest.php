<?php

use EncountersApi\Services\PartyNameService;

class PartyNameServiceTest extends TestCase
{
    private $service;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $this->service = new PartyNameService();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateHappyPath()
    {
        $this->assertNotNull($this->service->create());
    }

    public function testCreateAlreadyExists()
    {
        $existing = $this->service->create();
        factory(\EncountersApi\Party::class)->create([
            'hash'=> $existing
        ]);
        $this->assertNotEquals($existing, $this->service->create());
    }
}
