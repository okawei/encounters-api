<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AlignmentsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListAlignments()
    {
        $response = $this->json('GET', '/api/alignments');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'alignment'
            ]
        ]);
    }
}
