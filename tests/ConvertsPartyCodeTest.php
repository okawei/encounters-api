<?php

use EncountersApi\Http\Middleware\ConvertPartyCode;
use EncountersApi\Party;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ConvertsPartyCodeTest extends TestCase
{
    private $middleware;

    public function __construct($name = null, array $data = [], string $dataName = '')
    {
        $this->middleware = new ConvertPartyCode();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testConvertsSuccessfully()
    {
        $party = factory(Party::class)->create();
        $request = new Request([
            'party_code'=>$party->hash
        ]);
        $this->middleware->handle($request, function ($request) {
            $this->assertNotNull($request->get('party_id'));
        });
    }

    public function testThrowsErrorOnMismatch()
    {
        $this->expectException(ValidationException::class);
        $party = factory(Party::class)->create();
        $request = new Request([
            'party_code'=>$party->hash,
            'party_id'=>-10
        ]);
        $this->middleware->handle($request, function ($request) {
        });
    }

    public function testThrowsErrorOnInvalidPartyCode()
    {

        $this->expectException(ValidationException::class);
        $request = new Request([
            'party_code'=>str_random(100),
        ]);
        $this->middleware->handle($request, function ($request) {
        });
    }
}
