<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlayerClassesControllerTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListClasses()
    {
        $response = $this->json('GET', '/api/classes');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'class'
            ]
        ]);
    }
}
