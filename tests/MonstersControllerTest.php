<?php


class MonstersControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetMonsters()
    {
        $response = $this->json('GET', '/api/monsters');
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'monster'
            ]
        ]);
    }

    public function testGetMonstersList()
    {
        $party = factory(\EncountersApi\Party::class)->create();
        $response = $this->json('GET', '/api/monsters?list=true&party_code='.$party->hash);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            '*'=>[
                'id',
                'name'
            ]
        ]);
    }

    public function testCreateMonster(){
        $party = factory(\EncountersApi\Party::class)->create();
        $monster = factory(\EncountersApi\Monster::class)->make();
        $monster = (array)$monster->monster;
        $response = $this->json('POST', '/api/monsters?party_code='.$party->hash, $monster);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'monster'=>[
                "name",
                "size",
                "type",
                "speed",
                "senses",
                "wisdom",
                "actions",
                "insight",
                "stealth",
                "subtype",
                "charisma",
                "hit_dice",
                "strength",
                "alignment",
                "dexterity",
                "languages",
                "hit_points",
                "perception",
                "persuasion",
                "armor_class",
                "wisdom_save",
                "constitution",
                "intelligence",
                "charisma_save",
                "dexterity_save",
                "challenge_rating",
                "constitution_save",
                "damage_immunities",
                "legendary_actions",
                "special_abilities",
                "damage_resistances",
                "condition_immunities",
                "damage_vulnerabilities"
            ],
            'id'
        ]);
    }

    public function testUpdateMonster(){
        $party = factory(\EncountersApi\Party::class)->create();
        $monster = factory(\EncountersApi\Monster::class)->create([
            'party_id' => $party->id
        ]);
        $updates = factory(\EncountersApi\Monster::class)->make();
        $updates = (array)$updates->monster;
        $response = $this->json('PUT', '/api/monsters/'.$monster->id.'?party_code='.$party->hash, $updates);
        $response->assertResponseOk();
        $response->seeJsonStructure([
            'monster'=>[
                "name",
                "size",
                "type",
                "speed",
                "senses",
                "wisdom",
                "actions",
                "insight",
                "stealth",
                "subtype",
                "charisma",
                "hit_dice",
                "strength",
                "alignment",
                "dexterity",
                "languages",
                "hit_points",
                "perception",
                "persuasion",
                "armor_class",
                "wisdom_save",
                "constitution",
                "intelligence",
                "charisma_save",
                "dexterity_save",
                "challenge_rating",
                "constitution_save",
                "damage_immunities",
                "legendary_actions",
                "special_abilities",
                "damage_resistances",
                "condition_immunities",
                "damage_vulnerabilities"
            ],
            'id'
        ]);
    }
}
