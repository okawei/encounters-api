<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    use DatabaseTransactions;
    protected $faker;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $this->faker = \Faker\Factory::create();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
