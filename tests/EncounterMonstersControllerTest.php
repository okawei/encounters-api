<?php

use EncountersApi\Encounter;
use EncountersApi\EncounterMonster;
use EncountersApi\Monster;

class EncounterMonstersControllerTest extends TestCase
{
    const BASE_PATH = '/api/encounter/monsters/';

    public function testIndex()
    {
        $encounter = factory(Encounter::class)->create();
        $monster = Monster::inRandomOrder()->first();
        $encounterMonster = factory(EncounterMonster::class)->create([
            'encounter_id'=>$encounter->id,
            'monster_id'=>$monster->id
        ]);
        $response = $this->json('GET', self::BASE_PATH, [
            'party_code'=>$encounter->party->hash,
            'encounter_id' => $encounter->id
        ]);
        $response->assertResponseOk();
        collect(json_decode($response->response->content()))->each(function ($monsterCheck) use ($encounterMonster) {
            $this->assertEquals($monsterCheck->id, $encounterMonster->id);
        });
    }

    public function testStore()
    {
        $encounter = factory(Encounter::class)->create();
        $monster = Monster::inRandomOrder()->first();
        $response = $this->json('POST', self::BASE_PATH, [
            'encounter_id' => $encounter->id,
            'party_code' => $encounter->party->hash,
            'monster_id' => $monster->id
        ]);
        $response->assertResponseOk();
        $check = $encounter->monsters()->get()->first();
        $this->assertEquals($check->monster_id, $monster->id);
    }

    public function testShow()
    {
        $encounterMonster = factory(EncounterMonster::class)->create();
        $response = $this->json('GET', self::BASE_PATH.$encounterMonster->id, [
            'encounter_id'=>$encounterMonster->encounter_id,
            'party_code'=>$encounterMonster->encounter->party->hash
        ]);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());
        $this->assertEquals($responseData->id, $encounterMonster->id);
    }

    public function testUpdate()
    {
        $encounterMonster = factory(EncounterMonster::class)->create();
        $newFields = factory(EncounterMonster::class)->make([
            'encounter_id'=>$encounterMonster->encounter_id,
            'monster_id'=>$encounterMonster->monster_id
        ])->toArray();

        $response = $this->json('PUT', self::BASE_PATH.$encounterMonster->id, [
            'encounter_id'=>$encounterMonster->encounter_id,
            'party_code' => $encounterMonster->encounter->party->hash
        ] + $newFields);
        $response->assertResponseOk();
        $responseData = json_decode($response->response->content());

        foreach ($newFields as $key => $field) {
             $this->assertEquals($responseData->$key, $field);
        }
    }

    public function testDelete()
    {
        $encounterMonster = factory(EncounterMonster::class)->create();
        $response = $this->json('DELETE', self::BASE_PATH.$encounterMonster->id, [
            'encounter_id'=>$encounterMonster->encounter_id,
            'party_code'=>$encounterMonster->encounter->party->hash
        ]);
        $response->assertResponseOk();
        $this->assertTrue(EncounterMonster::find($encounterMonster->id) == null);
    }
}
