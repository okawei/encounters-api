<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace EncountersApi{
/**
 * EncountersApi\Event
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $encounter_id
 * @property string $text
 * @property-read \EncountersApi\Encounter $encounter
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Event whereEncounterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Event whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Event whereUpdatedAt($value)
 */
	class Event extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Alignment
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $alignment
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Alignment whereAlignment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Alignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Alignment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Alignment whereUpdatedAt($value)
 */
	class Alignment extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Spell
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property mixed $spell
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Spell whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Spell whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Spell whereSpell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Spell whereUpdatedAt($value)
 */
	class Spell extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\EncounterMonster
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $encounter_id
 * @property int $monster_id
 * @property int $initiative
 * @property int $hp
 * @property int $saving_pass
 * @property int $saving_fail
 * @property string|null $notes
 * @property string|null $status_effects
 * @property int $number
 * @property-read \EncountersApi\Encounter $encounter
 * @property-read \EncountersApi\Monster $monster
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereEncounterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereHp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereInitiative($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereMonsterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereSavingFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereSavingPass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereStatusEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterMonster whereUpdatedAt($value)
 */
	class EncounterMonster extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\EncounterPlayerCharacter
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $encounter_id
 * @property int $player_character_id
 * @property int $initiative
 * @property int $hp
 * @property int $saving_pass
 * @property int $saving_fail
 * @property string|null $notes
 * @property string|null $status_effects
 * @property-read \EncountersApi\Encounter $encounter
 * @property-read \EncountersApi\PlayerCharacter $playerCharacter
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereEncounterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereHp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereInitiative($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter wherePlayerCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereSavingFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereSavingPass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereStatusEffects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\EncounterPlayerCharacter whereUpdatedAt($value)
 */
	class EncounterPlayerCharacter extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Party
 *
 * @property int $id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $hash
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\Encounter[] $encounters
 * @property-read \EncountersApi\Encounter $latestEncounter
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\PlayerCharacter[] $playerCharacters
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Party onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Party whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Party whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Party whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Party whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Party whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Party withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Party withoutTrashed()
 */
	class Party extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Race
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $race
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Race whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Race whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Race whereRace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Race whereUpdatedAt($value)
 */
	class Race extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Encounter
 *
 * @property int $id
 * @property string|null $deleted_at
 * @property int $party_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\EncounterMonster[] $monsters
 * @property-read \EncountersApi\Party $party
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\EncounterPlayerCharacter[] $playerCharacters
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Encounter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Encounter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Encounter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Encounter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Encounter wherePartyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Encounter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Encounter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\Encounter withoutTrashed()
 */
	class Encounter extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\Monster
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property mixed $monster
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\Encounter[] $encounters
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Monster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Monster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Monster whereMonster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\Monster whereUpdatedAt($value)
 */
	class Monster extends \Eloquent {}
}

namespace EncountersApi{
/**
 * EncountersApi\PlayerCharacter
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $name
 * @property string $player_name
 * @property int $level
 * @property int $alignment_id
 * @property int $race_id
 * @property int|null $exp
 * @property int|null $gold
 * @property int|null $encumberment
 * @property int $party_id
 * @property int $str
 * @property int $dex
 * @property int $con
 * @property int $int
 * @property int $wis
 * @property int $cha
 * @property int $ac
 * @property int $max_hp
 * @property int $current_hp
 * @property int $initiative_modifier
 * @property int $speed
 * @property int|null $saving_pass
 * @property int|null $saving_fail
 * @property int $inspiration
 * @property int|null $proficiency_bonus
 * @property int|null $str_saving
 * @property int|null $dex_saving
 * @property int|null $con_saving
 * @property int|null $int_saving
 * @property int|null $wis_saving
 * @property int|null $cha_saving
 * @property-read \EncountersApi\Alignment $alignment
 * @property-read \Illuminate\Database\Eloquent\Collection|\EncountersApi\Encounter[] $encounters
 * @property-read \EncountersApi\Party $party
 * @property-read \EncountersApi\Race $race
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\PlayerCharacter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereAc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereAlignmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereCha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereChaSaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereCon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereConSaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereCurrentHp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereDex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereDexSaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereEncumberment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereGold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereInitiativeModifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereInspiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereInt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereIntSaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereMaxHp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter wherePartyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter wherePlayerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereProficiencyBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereSavingFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereSavingPass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereStrSaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereWis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\EncountersApi\PlayerCharacter whereWisSaving($value)
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\PlayerCharacter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\EncountersApi\PlayerCharacter withoutTrashed()
 */
	class PlayerCharacter extends \Eloquent {}
}

