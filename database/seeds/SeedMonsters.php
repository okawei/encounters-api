<?php

use EncountersApi\Monster;
use Illuminate\Database\Seeder;

class SeedMonsters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Monster::count()) {
            return;
        }

        $monsterReference = __DIR__.'/../../resources/monsters.json';
        $monsters = json_decode(file_get_contents($monsterReference), true);
        foreach ($monsters as $monster) {
            if (isset($monster['license'])) {
                continue;
            }
            Monster::create([
                'monster'=>json_encode($monster)
            ]);
        }
    }
}
