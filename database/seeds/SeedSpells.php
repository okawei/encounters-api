<?php

use EncountersApi\Spell;
use Illuminate\Database\Seeder;

class SeedSpells extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Spell::count()) {
            return;
        }
        $spellReference = __DIR__.'/../../resources/spells.json';
        $spells = json_decode(file_get_contents($spellReference), true);
        foreach ($spells as $spell) {
            if (isset($spell['license'])) {
                continue;
            }
            Spell::create([
                'spell'=>json_encode($spell)
            ]);
        }
    }
}
