<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->call(SeedMonsters::class);
        $this->call(SeedSpells::class);
        $this->call(SeedRaces::class);
        $this->call(SeedAlignments::class);
        $this->call(SeedClasses::class);
    }
}
