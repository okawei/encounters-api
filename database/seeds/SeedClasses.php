<?php

use EncountersApi\PlayerClass;
use Illuminate\Database\Seeder;

class SeedClasses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (PlayerClass::count()) {
            return;
        }
        $classes = [
            'Barbarian',
            'Bard',
            'Cleric',
            'Druid',
            'Fighter',
            'Monk',
            'Paladin',
            'Ranger',
            'Rogue',
            'Sorcerer',
            'Warlock',
            'Wizard',
        ];
        foreach ($classes as $class) {
            PlayerClass::create([
                'class'=>$class
            ]);
        }
    }
}
