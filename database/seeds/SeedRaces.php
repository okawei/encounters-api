<?php

use EncountersApi\Race;
use Illuminate\Database\Seeder;

class SeedRaces extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Race::count()) {
            return;
        }
        $races = [
            'Aarakocra',
            'Aasimar',
            'Bugbear',
            'Dragonborn',
            'Dwarf',
            'Elf',
            'Feral Tiefling',
            'Firbolg',
            'Genasi',
            'Gnome',
            'Goblin',
            'Goliath',
            'Half-Elf',
            'Halfling',
            'Half-Orc',
            'Hobgoblin',
            'Human',
            'Kenku',
            'Kobold',
            'Lizardfolk',
            'Orc',
            'Tabaxi',
            'Tiefling',
            'Tortle',
            'Triton',
            'Yuan-ti Pureblood',
        ];
        foreach ($races as $race) {
            Race::create([
                'race'=>$race
            ]);
        }
    }
}
