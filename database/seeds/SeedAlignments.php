<?php

use EncountersApi\Alignment;
use Illuminate\Database\Seeder;

class SeedAlignments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Alignment::count()) {
            return;
        }

        $row1 = ['Chaotic', 'Neutral', 'Lawful'];
        $row2 = ['Evil', 'Neutral', 'Good'];
        foreach ($row1 as $al1) {
            foreach ($row2 as $al2) {
                Alignment::create([
                    'alignment' => $al1 . ' ' . $al2
                ]);
            }
        }
    }
}
