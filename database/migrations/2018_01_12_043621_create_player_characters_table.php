<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_characters', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name');
            $table->string('player_name')->nullable();
            $table->integer('level')->nullable();
            $table->integer('alignment_id')->nullable();
            $table->integer('race_id')->nullable();
            $table->integer('class_id')->nullable();
            $table->integer('exp')->nullable();
            $table->integer('gold')->nullable();
            $table->integer('encumberment')->nullable();
            $table->integer('party_id');

            $table->integer('str')->default(10);
            $table->integer('dex')->default(10);
            $table->integer('con')->default(10);
            $table->integer('int')->default(10);
            $table->integer('wis')->default(10);
            $table->integer('cha')->default(10);
            $table->integer('ac')->nullable();
            $table->integer('max_hp')->default(9);
            $table->integer('current_hp')->default(9);
            $table->integer('initiative_modifier')->default(0);
            $table->integer('speed')->default(30);
            $table->integer('saving_pass')->nullable();
            $table->integer('saving_fail')->nullable();
            $table->integer('inspiration')->default(0);
            $table->integer('proficiency_bonus')->nullable();

            $table->integer('str_saving')->nullable();
            $table->integer('dex_saving')->nullable();
            $table->integer('con_saving')->nullable();
            $table->integer('int_saving')->nullable();
            $table->integer('wis_saving')->nullable();
            $table->integer('cha_saving')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_characters');
    }
}
