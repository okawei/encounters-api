<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncounterMonstersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encounter_monsters', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('encounter_id');
            $table->integer('monster_id');
            $table->integer('initiative')->default(0);
            $table->integer('hp');
            $table->integer('saving_pass')->default(0);
            $table->integer('saving_fail')->default(0);
            $table->text('notes')->nullable();
            $table->string('status_effects')->nullable();
            $table->integer('number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encounter_monsters');
    }
}
