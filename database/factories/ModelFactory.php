<?php

use EncountersApi\Alignment;
use EncountersApi\Enum\ChallengeRatings;
use EncountersApi\Enum\Conditions;
use EncountersApi\Enum\DamageTypes;
use EncountersApi\Enum\Dice;
use EncountersApi\Enum\Languages;
use EncountersApi\Enum\MonsterTypes;
use EncountersApi\Enum\Sizes;
if(!function_exists('getRandomActions')) {

    /**
     * @param \Faker\Generator $faker
     * @return array
     */
    function getRandomActions(Faker\Generator $faker, $legendary = false): array
    {
        $monsters = \EncountersApi\Monster::inRandomOrder()->take($faker->numberBetween(2, 4))->get();
        $actionCount = $faker->numberBetween(1, 3);
        $actions = [];
        foreach ($monsters as $monster) {
            if (isset($monster->monster->legendary_actions) && $legendary) {
                $actions[] = $faker->randomElement($monster->monster->actions);
            } else if (isset($monster->monster->actions) && !$legendary) {
                $actions[] = $faker->randomElement($monster->monster->actions);
            }else {
                continue;
            }

            if (count($actions) == $actionCount) {
                break;
            }
        }
        return $actions;
    }
}

if(!function_exists('getRandomSpecialAbilities')) {


    /**
     * @param \Faker\Generator $faker
     * @return array
     */
    function getRandomSpecialAbilities(Faker\Generator $faker): array
    {
        $monsters = \EncountersApi\Monster::inRandomOrder()->take($faker->numberBetween(2, 4))->get();
        $actionCount = $faker->numberBetween(1, 2);
        $actions = [];
        foreach ($monsters as $monster) {
            if (!isset($monster->monster->special_abilities)) {
                continue;
            }
            $actions[] = $faker->randomElement($monster->monster->special_abilities);
            if (count($actions) == $actionCount) {
                break;
            }
        }
        return $actions;
    }
}

$factory->define(EncountersApi\Alignment::class, function (Faker\Generator $faker) {
    return [
        'alignment' => $faker->word,
    ];
});

$factory->define(EncountersApi\Spell::class, function (Faker\Generator $faker) {
    return [
        'spell' => 'json_array',
    ];
});

$factory->define(EncountersApi\EncounterMonster::class, function (Faker\Generator $faker) {
    return [
        'encounter_id' => function () {
             return factory(EncountersApi\Encounter::class)->create()->id;
        },
        'monster_id' => function () {
             return \EncountersApi\Monster::inRandomOrder()->first()->id;
        },
        'initiative' => $faker->randomNumber(),
        'hp' => $faker->randomNumber(),
        'saving_pass' => $faker->randomNumber(),
        'saving_fail' => $faker->randomNumber(),
        'notes' => $faker->text,
        'status_effects' => $faker->word,
        'number' => $faker->randomNumber(),
    ];
});

$factory->define(EncountersApi\EncounterPlayerCharacter::class, function (Faker\Generator $faker) {
    return [
        'encounter_id' => function () {
             return factory(EncountersApi\Encounter::class)->create()->id;
        },
        'player_character_id' => function () {
             return factory(EncountersApi\PlayerCharacter::class)->create()->id;
        },
        'initiative' => $faker->randomNumber(),
        'hp' => $faker->randomNumber(),
        'saving_pass' => $faker->randomNumber(),
        'saving_fail' => $faker->randomNumber(),
        'notes' => $faker->text,
        'status_effects' => $faker->word,
    ];
});

$factory->define(EncountersApi\Party::class, function (Faker\Generator $faker) {
    return [
        'hash' => str_random(32),
    ];
});

$factory->define(EncountersApi\Race::class, function (Faker\Generator $faker) {
    return [
        'race' => $faker->word,
    ];
});

$factory->define(EncountersApi\Encounter::class, function (Faker\Generator $faker) {
    return [
        'party_id' => function () {
             return factory(EncountersApi\Party::class)->create()->id;
        },
    ];
});

$factory->define(EncountersApi\Monster::class, function (Faker\Generator $faker) {
    $rpgFaker = new RPGFaker\RPGFaker();


    $actions = getRandomActions($faker);
    $legendaryActions = $faker->boolean ? getRandomActions($faker, true) : [];
    $specialAbilities = $faker->boolean ? getRandomSpecialAbilities($faker) : [];
    return [
        'monster' => json_encode([
            "name"=>$rpgFaker->name,
            "size"=>$faker->randomElement(Sizes::SIZES),
            "type"=>$faker->randomElement(MonsterTypes::TYPES),
            "speed"=>$faker->numberBetween(1, 15)*10 . 'ft',
            "senses"=>implode(', ', $faker->randomElements(\EncountersApi\Enum\Senses::SENSES, $faker->numberBetween(0,2))),
            "wisdom"=>$faker->numberBetween(1, 30),
            "actions"=>$actions,
            "insight"=>$faker->numberBetween(1, 30),
            "stealth"=>$faker->numberBetween(1, 30),
            "subtype"=>$faker->boolean ? $faker->randomElement(MonsterTypes::TYPES) : '',
            "charisma"=>$faker->numberBetween(1, 30),
            "hit_dice"=>$faker->numberBetween(1, 24).$faker->randomElement(Dice::DICE),
            "strength"=>$faker->numberBetween(1, 30),
            "alignment"=> Alignment::inRandomOrder()->first()->alignment,
            "dexterity"=>$faker->numberBetween(1, 30),
            "languages"=>implode(', ', $faker->randomElements(Languages::LANGUAGES, $faker->numberBetween(1, 3))),
            "hit_points"=>$faker->numberBetween(2, 676),
            "perception"=>$faker->numberBetween(2, 30),
            "persuasion"=>$faker->numberBetween(2, 30),
            "armor_class"=>$faker->numberBetween(1, 25),
            "wisdom_save"=>$faker->numberBetween(-5, 8),
            "constitution"=>$faker->numberBetween(2, 30),
            "intelligence"=>$faker->numberBetween(2, 30),
            "charisma_save"=>$faker->numberBetween(-5, 8),
            "dexterity_save"=>$faker->numberBetween(-5, 8),
            "challenge_rating"=>$faker->randomElement(ChallengeRatings::CHALLENGE_RATINGS),
            "constitution_save"=>$faker->numberBetween(-5, 8),
            "damage_immunities"=>implode(', ', $faker->randomElements(DamageTypes::DAMAGE_TYPES, $faker->numberBetween(0, 3))),
            "legendary_actions"=>$legendaryActions,
            "special_abilities"=>$specialAbilities,
            "damage_resistances"=>$faker->boolean ? implode(', ', $faker->randomElements(DamageTypes::DAMAGE_TYPES, $faker->numberBetween(0, 3))) : '',
            "condition_immunities"=>$faker->boolean ? implode(', ', $faker->randomElements(Conditions::CONDITIONS, $faker->numberBetween(0, 3))) : '',
            "damage_vulnerabilities"=>implode(', ', $faker->randomElements(DamageTypes::DAMAGE_TYPES, $faker->numberBetween(0, 3)))
        ]),
    ];
});

$factory->define(EncountersApi\PlayerCharacter::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'player_name' => $faker->word,
        'level' => $faker->randomNumber(),
        'alignment_id' => $faker->randomNumber(),
        'race_id' => $faker->randomNumber(),
        'exp' => $faker->randomNumber(),
        'gold' => $faker->randomNumber(),
        'encumberment' => $faker->randomNumber(),
        'party_id' => function () {
             return factory(EncountersApi\Party::class)->create()->id;
        },
        'str' => $faker->randomNumber(),
        'dex' => $faker->randomNumber(),
        'con' => $faker->randomNumber(),
        'int' => $faker->randomNumber(),
        'wis' => $faker->randomNumber(),
        'cha' => $faker->randomNumber(),
        'ac' => $faker->randomNumber(),
        'max_hp' => $faker->randomNumber(),
        'current_hp' => $faker->randomNumber(),
        'initiative_modifier' => $faker->randomNumber(),
        'speed' => $faker->randomNumber(),
        'saving_pass' => $faker->randomNumber(),
        'saving_fail' => $faker->randomNumber(),
        'inspiration' => $faker->randomNumber(),
        'proficiency_bonus' => $faker->randomNumber(),
        'str_saving' => $faker->randomNumber(),
        'dex_saving' => $faker->randomNumber(),
        'con_saving' => $faker->randomNumber(),
        'int_saving' => $faker->randomNumber(),
        'wis_saving' => $faker->randomNumber(),
        'cha_saving' => $faker->randomNumber(),
    ];
});

$factory->define(EncountersApi\Event::class, function (Faker\Generator $faker) {
    return [
        'encounter_id' => function () {
             return factory(EncountersApi\Encounter::class)->create()->id;
        },
        'text' => $faker->word,
    ];
});
