<?php
namespace EncountersApi\Traits;

use EncountersApi\Encounter;
use Illuminate\Http\Request;

trait FetchesEncounters
{
    /**
     * @param Request $request
     * @param $id
     */
    private function validateAndGetEncounterForId(Request $request, $id = null): Encounter
    {
        $encounter = $this->validateAndGetEncounterBuilder($request, $id);
        return $encounter->firstOrFail();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    private function validateAndGetEncounterBuilder(Request $request, $id)
    {
        $this->validate($request, [
            'party_id' => 'required'
        ]);
        if ($id == null) {
            $this->validate($request, [
                'encounter_id' => 'required'
            ]);
        }
        $id = $id ?: $request->get('encounter_id');

        $encounter = Encounter::where('party_id', $request->get('party_id'))
            ->whereId($id);
        ;
        return $encounter;
    }
}
