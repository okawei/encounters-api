<?php
namespace EncountersApi\Services;

use EncountersApi\Party;
use Faker\Factory;

class PartyNameService
{
    /**
     * @var array
     */
    private $verbs;
    /**
     * @var array
     */
    private $adjectives;
    /**
     * @var array
     */
    private $nouns;

    /**
     * PartyNameService constructor.
     */
    public function __construct()
    {
        $this->verbs = $this->loadArray(__DIR__.'/../../resources/verbs.json');
        $this->nouns = $this->loadArray(__DIR__.'/../../resources/nouns.json');
        $this->adjectives = $this->loadArray(__DIR__.'/../../resources/adjectives.json');
    }

    private function loadArray($filename)
    {
        return collect(json_decode(file_get_contents($filename), true));
    }

    public function create()
    {
        $faker = Factory::create();
        $check = null;
        while ($check == null || Party::whereHash($check)->exists()) {
            list($verbs, $nouns, $adjectives) = $this->filterListsByBaseLetter();
            $check = ucfirst($faker->randomElement($adjectives)).
                    ucfirst($faker->randomElement($verbs)).
                     ucfirst($faker->randomElement($nouns));
        }
        return $check;
    }

    /**
     * @param $baseLetter
     * @return array
     */
    private function filterListsByBaseLetter()
    {
        $baseLetter = strtolower(str_random(1));
        $filterByBaseLetter = function ($word) use ($baseLetter) {
            return substr($word, 0, 1) == $baseLetter;
        };
        $verbsAvailable = $this->verbs->filter($filterByBaseLetter)->toArray();
        $nounsAvailable = $this->nouns->filter($filterByBaseLetter)->toArray();
        $adjectivesAvailable = $this->adjectives->filter($filterByBaseLetter)->toArray();

        return [$verbsAvailable, $nounsAvailable, $adjectivesAvailable];
    }
}
