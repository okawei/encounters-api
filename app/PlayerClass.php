<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;

class PlayerClass extends Model
{
    protected $guarded = ['id'];
}
