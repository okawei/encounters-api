<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;

class Alignment extends Model
{
    protected $guarded = ['id'];
}
