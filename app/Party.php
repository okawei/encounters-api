<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Party extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playerCharacters()
    {
        return $this->hasMany(PlayerCharacter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function encounters()
    {
        return $this->hasMany(Encounter::class);
    }

    /**
     * @return HasOne
     */
    public function latestEncounter()
    {
        return $this->hasOne(Encounter::class)->orderBy('created_at', 'desc');
    }

    /**
     * @return array|static
     */
    public function latestEncounterEntities()
    {
        if (!$this->latestEncounter) {
            return [];
        }
        /** @var Collection $monsters */
        $monsters = EncounterMonster::where('encounter_id', $this->latestEncounter->id)->with('monster')->get();
        $players = EncounterPlayerCharacter::where('encounter_id', $this->latestEncounter->id)->with('playerCharacter')->get();
        $entities = $monsters->merge($players);
        $entities = $entities->sort(function ($a, $b) {
            return $a->initiative < $b->initiative;
        });
        return array_values($entities->toArray());
    }
}
