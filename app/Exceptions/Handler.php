<?php

namespace EncountersApi\Exceptions;

use EncountersApi\Http\Responses\FailedValidationResponse;
use EncountersApi\Http\Responses\NotFoundResponse;
use EncountersApi\Http\Responses\Response as CustomResponse;
use Exception;
use Illuminate\Auth\Events\Failed;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zend\Diactoros\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // Check if we're already returning a custom response
        if (method_exists($e, 'getResponse')) {
            if ($e->getResponse() instanceof CustomResponse) {
                return $e->getResponse();
            }
        }

        if ($e instanceof ValidationException) {
            return new FailedValidationResponse($e->validator->errors());
        }
        if ($e instanceof NotFoundHttpException) {
            $message = 'The path ' . $request->getUri() . ' does not exist';
            return new NotFoundResponse($message);
        }

        // We don't want to handle custom responses as json
        return parent::render($request, $e);
    }
}
