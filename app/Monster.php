<?php

namespace EncountersApi;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Monster extends Model
{
    protected $fillable = [
        'monster'
    ];

    protected $appends = [
        'is_custom'
    ];

    protected $hidden = [
        'party_id'
    ];

    public function getMonsterAttribute($monster)
    {
        return json_decode($monster);
    }

    public function encounters()
    {
        return $this->belongsToMany(Encounter::class);
    }

    public function getIsCustomAttribute(){
        return $this->party_id != 0;
    }
}
