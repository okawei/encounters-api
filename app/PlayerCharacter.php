<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlayerCharacter extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    protected $hidden = [
        'party_id'
    ];

    public function party()
    {
        return $this->belongsTo(Party::class);
    }

    public function encounters()
    {
        return $this->belongsToMany(Encounter::class);
    }

    public function alignment()
    {
        return $this->belongsTo(Alignment::class);
    }

    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    public function class()
    {
        return $this->belongsTo(PlayerClass::class);
    }
}
