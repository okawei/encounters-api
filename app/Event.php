<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [
        'id'
    ];

    public function encounter()
    {
        return $this->belongsTo(Encounter::class);
    }
}
