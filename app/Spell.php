<?php

namespace EncountersApi;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Spell extends Model
{
    protected $fillable = [
        'spell'
    ];

    public function getSpellAttribute($spell)
    {
        return json_decode($spell);
    }
}
