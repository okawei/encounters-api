<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;

class EncounterPlayerCharacter extends Model
{
    protected $guarded = [
        'id'
    ];


    protected $appends = [
        'type'
    ];

    public function encounter()
    {
        return $this->belongsTo(Encounter::class);
    }

    public function playerCharacter()
    {
        return $this->belongsTo(PlayerCharacter::class);
    }

    public function getTypeAttribute()
    {
        return 'player_character';
    }
}
