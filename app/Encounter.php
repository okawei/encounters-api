<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Encounter extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id'
    ];
    protected $hidden = [
        'party_id'
    ];

    public function party()
    {
        return $this->belongsTo(Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playerCharacters()
    {
        return $this->hasMany(EncounterPlayerCharacter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function monsters()
    {
        return $this->hasMany(EncounterMonster::class);
    }
}
