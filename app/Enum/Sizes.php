<?php
namespace EncountersApi\Enum;

class Sizes{
    const SIZES = [
        'Fine',
        'Diminutive',
        'Tiny',
        'Small',
        'Medium',
        'Large',
        'Huge',
        'Gargantuan',
        'Colossal',
        'Colossal+',
    ];
}