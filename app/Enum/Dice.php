<?php

namespace EncountersApi\Enum;

class Dice{
    const DICE = [
        'd20',
        'd12',
        'd10',
        'd8',
        'd6',
        'd4'
    ];
}