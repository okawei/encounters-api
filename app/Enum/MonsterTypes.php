<?php
namespace  EncountersApi\Enum;

class MonsterTypes{
    const TYPES = [
        "Aberration",
        "Beast",
        "Celestial",
        "Construct",
        "Dragon",
        "Elemental",
        "Fey",
        "Fiend",
        "Fiend (Demon)",
        "Fiend (Devil)",
        "Fiend (Shapechanger)",
        "Giant",
        "Humanoid (Any Race)",
        "Humanoid (Dwarf)",
        "Humanoid (Elf)",
        "Humanoid (Gnoll)",
        "Humanoid (Gnome)",
        "Humanoid (Goblinoid)",
        "Humanoid (Grimlock)",
        "Humanoid (Human)",
        "Humanoid (Human, Shapechanger)",
        "Humanoid (Kobold)",
        "Humanoid (Lizardfolk)",
        "Humanoid (Merfolk)",
        "Humanoid (Orc)",
        "Humanoid (Sahuagin)",
        "Monstrosity",
        "Monstrosity (Shapechanger)",
        "Monstrosity (Titan)",
        "Ooze",
        "Plant",
        "Swarm Of Tiny Beasts",
        "Undead",
        "Undead (Shapechanger)"
    ];
}