<?php

namespace EncountersApi\Enum;

class Languages{
    const LANGUAGES = [
        'Abyssal',
        'Aquan',
        'Auran',
        'Celestial',
        'Common',
        'Deep Speech',
        'Draconic',
        'Druidic',
        'Dwarvish',
        'Elvish',
        'Giant',
        'Gnomish',
        'Goblin',
        'Gnoll',
        'Halfling',
        'Ignan',
        'Infernal',
        'Orc',
        'Primordial',
        'Sylvan',
        'Terran',
        'Undercommon'
    ];
}