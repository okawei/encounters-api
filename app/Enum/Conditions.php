<?php
namespace EncountersApi\Enum;

class Conditions{
    const CONDITIONS = [
        'Blinded',
        'Charmed',
        'Deafened',
        'Fatigued',
        'Frightened',
        'Grappled',
        'Incapacitated',
        'Invisible',
        'Paralyzed',
        'Petrified',
        'Poisoned',
        'Prone',
        'Restrained',
        'Stunned',
        'Unconscious',
        'Exhaustion',
    ];
}