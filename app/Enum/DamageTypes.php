<?php

namespace EncountersApi\Enum;
class DamageTypes{
    const DAMAGE_TYPES = [
        'Acid',
        'Bludgeoning',
        'Cold',
        'Fire',
        'Force',
        'Lightning',
        'Necrotic',
        'Piercing',
        'Poison',
        'Psychic',
        'Radiant',
        'Slashing',
        'Thunder',
    ];
}