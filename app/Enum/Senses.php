<?php
namespace EncountersApi\Enum;
class Senses{
    const SENSES = [
        'Blindsight',
        'Darkvision',
        'Tremorsense',
        'Truesight',
    ];
}