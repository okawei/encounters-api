<?php

namespace EncountersApi\Http\Middleware;

use Closure;
use EncountersApi\Http\Responses\FailedValidationResponse;
use EncountersApi\Party;
use Illuminate\Validation\ValidationException;

class ConvertPartyCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Pre-Middleware Action
        if ($request->get('party_code')) {
            $party = Party::whereHash($request->get('party_code'));
            $this->checkForPartyIdMismatch($request, $party);
            $this->confirmPartyExists($party);
            $this->setPartyId($request, $party);
        }
        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }

    /**
     * @param $request
     * @param $party
     * @throws ValidationException
     */
    private function checkForPartyIdMismatch($request, $party)
    {
        if ($request->get('party_id') != null && $request->get('party_id') != $party->first()->id) {
            throw new ValidationException(
                app('validator'),
                new FailedValidationResponse('The passed party code was invalid')
            );
        }
    }

    /**
     * @param $party
     * @throws ValidationException
     */
    private function confirmPartyExists($party)
    {
        if (!$party->exists()) {
            throw new ValidationException(
                app('validator'),
                new FailedValidationResponse('The passed party code was invalid')
            );
        }
    }

    /**
     * @param $request
     * @param $party
     */
    private function setPartyId($request, $party)
    {
        $party = $party->first();
        $request->offsetSet('party_id', $party->id);
        $request->offsetUnset('party_code');
    }
}
