<?php
namespace EncountersApi\Http\Responses;

class Response extends \Illuminate\Http\Response
{
    public function __construct($content = '', $status)
    {
        $headers = [
            'Content-Type'=>'application/json'
        ];
        parent::__construct($content, $status, $headers);
    }
}
