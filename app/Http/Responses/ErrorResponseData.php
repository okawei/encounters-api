<?php
namespace EncountersApi\Http\Responses;

use Illuminate\Support\MessageBag;

class ErrorResponseData implements \JsonSerializable
{

    private $code;
    private $message;

    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public static function create($code, $message)
    {
        if ($message instanceof MessageBag) {
            $message = collect($message)->map(function ($error) {
                return implode('. ', $error);
            })->implode(' ');
        }
        return new self($code, $message);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'success'=>false,
            'code'=>$this->code,
            'message'=>$this->message
        ];
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
