<?php
namespace EncountersApi\Http\Responses;

class NotFoundResponse extends Response
{
    public function __construct($message)
    {
        $status = 404;
        $content = json_encode(ErrorResponseData::create($status, $message));
        parent::__construct($content, $status);
    }
}
