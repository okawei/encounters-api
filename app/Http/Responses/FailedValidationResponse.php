<?php
namespace EncountersApi\Http\Responses;

class FailedValidationResponse extends Response
{
    public function __construct($message)
    {
        $status = 422;
        $data = ErrorResponseData::create($status, $message);
        $content = json_encode($data);
        parent::__construct($content, $status);
    }
}
