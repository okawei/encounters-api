<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Spell;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SpellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|static[]|Collection
     */
    public function index(Request $request)
    {

        if ($request->get('list')) {
            return Spell::all()->map(function ($spell) {
                return [
                    'id'=>$spell->id,
                    'name'=>$spell->spell->name
                ];
            });
        }
        return Spell::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Spell::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
