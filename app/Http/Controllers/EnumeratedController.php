<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Enum\ChallengeRatings;
use EncountersApi\Enum\Conditions;
use EncountersApi\Enum\DamageTypes;
use EncountersApi\Enum\Dice;
use EncountersApi\Enum\Languages;
use EncountersApi\Enum\MonsterTypes;
use EncountersApi\Enum\Senses;
use EncountersApi\Enum\Sizes;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EnumeratedController extends Controller
{

    public function getMapToMethod($method){
        $method = 'get'.camel_case($method);
        if(!method_exists($this, $method)){
            throw new NotFoundHttpException('The method '.$method.' does not exist');
        }
        return $this->$method();
    }

    public function getEnumerators(){
        return [
            'challenge_ratings' => $this->getChallengeRating(),
            'conditions' => $this->getConditions(),
            'damageTypes' => $this->getDamageTypes(),
            'dice' => $this->getDice(),
            'languages' => $this->getLanguages(),
            'monster_types' => $this->getMonsterTypes(),
            'senses' => $this->getSenses(),
            'sizes' => $this->getSizes()
        ];
    }

    public function getChallengeRating(){
        return ChallengeRatings::CHALLENGE_RATINGS;
    }

    public function getConditions(){
        return Conditions::CONDITIONS;
    }

    public function getDamageTypes(){
        return DamageTypes::DAMAGE_TYPES;
    }

    public function getDice(){
        return Dice::DICE;
    }

    public function getLanguages(){
        return Languages::LANGUAGES;
    }

    public function getMonsterTypes(){
        return MonsterTypes::TYPES;
    }

    public function getSenses(){
        return Senses::SENSES;
    }

    public function getSizes(){
        return Sizes::SIZES;
    }

}
