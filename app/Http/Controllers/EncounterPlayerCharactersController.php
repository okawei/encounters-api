<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Encounter;
use EncountersApi\EncounterPlayerCharacter;
use EncountersApi\PlayerCharacter;
use EncountersApi\Traits\FetchesEncounters;
use Illuminate\Http\Request;

class EncounterPlayerCharactersController extends Controller
{
    use FetchesEncounters;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return EncounterPlayerCharacter::where('encounter_id', $encounter->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        $playerCharacter = PlayerCharacter::whereId($request->get('player_character_id'))
            ->where('party_id', $request->get('party_id'))
            ->firstOrFail();
        $encounterPlayerCharacter = EncounterPlayerCharacter::create([
            'encounter_id'=>$encounter->id,
            'player_character_id'=>$playerCharacter->id,
            'hp'=>$playerCharacter->max_hp
        ]);
        return EncounterPlayerCharacter::where('id', $encounterPlayerCharacter->id)->with('playerCharacter')->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return $this->getEncounterPlayerCharacter($id, $encounter)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        $this->getEncounterPlayerCharacter($id, $encounter)->update($request->except('player_character', 'party_id', 'encounter_id', 'type'));
        return $this->getEncounterPlayerCharacter($id, $encounter)->with('playerCharacter')->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        $this->getEncounterPlayerCharacter($id, $encounter)->delete();
        return [
            'success' => true,
            'id' => $id
        ];
    }

    /**
     * @param $id
     * @param $encounter
     * @return mixed
     */
    private function getEncounterPlayerCharacter($id, $encounter)
    {
        return EncounterPlayerCharacter::where('id', $id)->where('encounter_id', $encounter->id);
    }
}
