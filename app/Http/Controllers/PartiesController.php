<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Party;
use EncountersApi\Services\PartyNameService;
use Faker\Factory;
use Illuminate\Http\Request;
use Nubs\RandomNameGenerator\Alliteration;

class PartiesController extends Controller
{
    const HASH_LENGTH = 16;
    /**
     * @var PartyNameService
     */
    private $partyNameService;

    public function __construct(PartyNameService $partyNameService)
    {
        $this->partyNameService = $partyNameService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Party
     */
    public function store(Request $request)
    {
        $party = new Party();
        $party->hash = $this->partyNameService->create();
        $party->save();
        return $party;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function show($id)
    {
        $party = Party::with('playerCharacters')->with('latestEncounter')->where('hash', $id)->first();
        $party->latest_encounter_entities = $party->latestEncounterEntities();
        return $party;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
