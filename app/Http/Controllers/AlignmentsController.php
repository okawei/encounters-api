<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Alignment;
use Illuminate\Http\Request;

class AlignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Alignment::all();
    }
}
