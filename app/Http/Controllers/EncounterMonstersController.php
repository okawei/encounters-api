<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\EncounterMonster;
use EncountersApi\Monster;
use EncountersApi\Traits\FetchesEncounters;
use Illuminate\Http\Request;

class EncounterMonstersController extends Controller
{
    use FetchesEncounters;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return EncounterMonster::where('encounter_id', $encounter->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'monster_id'=>'required'
        ]);
        $encounter = $this->validateAndGetEncounterForId($request);
        $monster = Monster::whereId($request->get('monster_id'))->firstOrFail();
        $number = $encounter->monsters()->where('monster_id', $request->get('monster_id'))->count();
        $encounterMonster = EncounterMonster::create([
            'encounter_id'=>$encounter->id,
            'monster_id'=>$monster->id,
            'hp'=>$monster->monster->hit_points,
            'number' => $number
        ]);
        return EncounterMonster::where('id', $encounterMonster->id)->with('monster')->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return $this->getEncounterMonster($id, $encounter)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        $this->getEncounterMonster($id, $encounter)->update($request->except('monster', 'party_id', 'encounter_id', 'type'));
        return $this->getEncounterMonster($id, $encounter)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        $this->getEncounterMonster($id, $encounter)->delete();
        return [
            'success' => true,
            'id' => $id
        ];
    }

    /**
     * @param $id
     * @param $encounter
     * @return mixed
     */
    private function getEncounterMonster($id, $encounter)
    {
        return EncounterMonster::where('id', $id)->where('encounter_id', $encounter->id);
    }
}
