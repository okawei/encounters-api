<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Event;
use EncountersApi\Traits\FetchesEncounters;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    use FetchesEncounters;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return Event::where('encounter_id', $encounter->id)->orderBy('updated_at')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return Event::create([
            'encounter_id'=>$encounter->id,
            'text' => $request->get('text')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return Event::where('encounter_id', $encounter->id)->where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $encounter = $this->validateAndGetEncounterForId($request);
        return Event::where('encounter_id', $encounter->id)->where('id', $id)->delete();
    }
}
