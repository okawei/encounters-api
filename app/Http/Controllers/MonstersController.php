<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Monster;
use Illuminate\Http\Request;
use Psr\Http\Message\ServerRequestInterface;

class MonstersController extends Controller
{
    const MONSTER_KEYS = [
        "name",
        "size",
        "type",
        "speed",
        "senses",
        "wisdom",
        "actions",
        "insight",
        "stealth",
        "subtype",
        "charisma",
        "hit_dice",
        "strength",
        "alignment",
        "dexterity",
        "languages",
        "hit_points",
        "perception",
        "persuasion",
        "armor_class",
        "wisdom_save",
        "constitution",
        "intelligence",
        "charisma_save",
        "dexterity_save",
        "challenge_rating",
        "constitution_save",
        "damage_immunities",
        "legendary_actions",
        "special_abilities",
        "damage_resistances",
        "condition_immunities",
        "damage_vulnerabilities"
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response|static[]
     */
    public function index(Request $request)
    {
        if ($request->get('list')) {
            return Monster::where('party_id', 0)
                ->orWhere('party_id', $request->get('party_id'))
                ->orderByRaw("monster->>'name' asc")
                ->get()
                ->map(function ($monster) {
                    return [
                        'id'=>$monster->id,
                        'name'=>$monster->monster->name
                    ];
                });
        }
        return Monster::where('party_id', 0)
            ->orWhere('party_id', $request->get('party_id'))
            ->orderByRaw("monster->>'name' asc")
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Monster
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'actions' => 'required|array',
            'party_id' => 'required'
        ]);

        $monster = new Monster();
        $monster->monster = json_encode($request->only(self::MONSTER_KEYS));
        $monster->party_id = $request->party_id;
        $monster->save();
        return $monster;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return Monster::where('id', $id)->where(function($q) use ($request){
            $q->where('party_id', 0)->orWhere('party_id', $request->get('party_id'));
        })->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Monster::where('id', $id)->where('party_id', $request->get('party_id'))->update([
            'monster'=>json_encode($request->only(self::MONSTER_KEYS))
        ]);
        $monster = Monster::where('id', $id)->where('party_id', $request->get('party_id'))->first();

        return $monster;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'party_id' => 'required'
        ]);
        Monster::where('id', $id)->where('party_id', $request->get('party_id'))->delete();
        return [
            'success' => true,
            'monster_id' => $id
        ];
    }
}
