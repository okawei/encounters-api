<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Encounter;
use EncountersApi\EncounterMonster;
use EncountersApi\EncounterPlayerCharacter;
use EncountersApi\Monster;
use EncountersApi\Party;
use EncountersApi\PlayerCharacter;
use EncountersApi\Traits\FetchesEncounters;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EncountersController extends Controller
{
    use FetchesEncounters;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'party_id'=>'required'
        ]);
        return Encounter::wherePartyId($request->get('party_id'))->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'party_id'=>'required'
        ]);
        return Encounter::create([
            'party_id'=>$request->get('party_id')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Encounter
     */
    public function show(Request $request, $id)
    {
        $encounter =  $this->validateAndGetEncounterBuilder($request, $id)->with('monsters', 'playerCharacters', 'monsters.monster')->first();
        /** @var Collection $members */
        $members = $encounter->monsters == null ? $encounter->playerCharacters : $encounter->monsters->merge($encounter->playerCharacters);
        $members = $members->sort(function ($a, $b) {
            return $a->initiative < $b->initiative;
        });
        $encounter->members = $members->values();
        return $encounter;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * @param Request $request
     * @param $encounterId
     * @return array
     */
    public function rollInitiative(Request $request, $encounterId){

        $encounter = $this->validateAndGetEncounterForId($request, $encounterId);

        foreach($request->get('entities') as $entity){
            if($entity['type'] == 'player_character'){
                EncounterPlayerCharacter::where('id', $entity['id'])->where('encounter_id', $encounter->id)->update([
                    'initiative'=>$entity['initiative']
                ]);
            } else if ($entity['type'] == 'monster'){
                EncounterMonster::where('id', $entity['id'])->where('encounter_id', $encounter->id)->update([
                    'initiative' => $entity['initiative']
                ]);
            }
        }

        /** @var Party $party */
        $party = Party::find($encounter->party_id);
        return $party->latestEncounterEntities();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return bool
     * @throws \Exception
     */
    public function destroy(Request $request, $id)
    {
        return $this->validateAndGetEncounterForId($request, $id)->delete();
    }
}
