<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\Race;
use Illuminate\Http\Request;

class RacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Race::all();
    }
}
