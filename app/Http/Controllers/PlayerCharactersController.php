<?php

namespace EncountersApi\Http\Controllers;

use EncountersApi\EncounterPlayerCharacter;
use EncountersApi\Party;
use EncountersApi\PlayerCharacter;
use Illuminate\Http\Request;

class PlayerCharactersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'party_id'=>'required'
        ]);
        return PlayerCharacter::whereHas('party', function ($q) use ($request) {
            $q->where('id', $request->get('party_id'));
        })->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'party_id'=>'required'
        ]);
        return PlayerCharacter::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->validate($request, [
            'party_id'=>'required'
        ]);
        return PlayerCharacter::where('id', $id)->with('race', 'alignment', 'class')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        EncounterPlayerCharacter::where('player_character_id', $id)->update([
            'hp' => $request->get('max_hp')
        ]);
        return [
            'success' => PlayerCharacter::find($id)->update($request->except('race', 'alignment', 'class')),
            'id' => $id,
            'player_character' => $request->all()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        EncounterPlayerCharacter::where('player_character_id', $id)->delete();
        return [
            'success'=>PlayerCharacter::find($id)->delete(),
            'id' => $id
            ];
    }
}
