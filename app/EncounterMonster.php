<?php

namespace EncountersApi;

use Illuminate\Database\Eloquent\Model;

class EncounterMonster extends Model
{
    protected $guarded = [
        'id'
    ];

    protected $appends = [
        'type'
    ];

    public function encounter()
    {
        return $this->belongsTo(Encounter::class);
    }

    public function monster()
    {
        return $this->belongsTo(Monster::class);
    }

    public function getTypeAttribute()
    {
        return 'monster';
    }
}
